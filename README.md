# Unity Http Web Request (SCC)

unity package for http web request in scc game

## Installation

double click the package

## Usage

```python
using XGamingAPI

# returns title data JSON string // Asynchronous
StartCoroutine(HttpReqManager.Instance.IERequestTitleDataAsync(EHTTPReqMethod.POST, titleKey, (result) =>
            {

                Debug.Log("Result II: " + result);

            }));

# returns title data JSON string // CustomCall
private IEnumerator CustomCall()

        {
            bool isFinish = false; # boolean flag for wait the process
            HttpReqManager.Instance.RequestTitleData(EHTTPReqMethod.POST, titleKey, (result) =>
            {
                Debug.Log("Result III : " + result);
                isFinish = true;
            });

            yield return new WaitUntil(() => isFinish == true);
        }

# then call this

StartCoroutine(CustomCall());
```
## Example
Set Example prefab and set HttpReqManager prefab at the same scene then run. Watch log.

## Contributing


## License
[MIT](https://choosealicense.com/licenses/mit/)